import mysql from "mysql2/promise";
import log from "@ajar/marker";

const connection = await mysql.createConnection({
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  database: process.env.DB_NAME,
  user: process.env.DB_USER_NAME,
  password: process.env.DB_USER_PASSWORD,
});
log.magenta(" ✨  Connected to MySQL ✨ ");

export default connection;

// const [rows, fields] = await db.execute('SELECT * FROM `table` WHERE `user_id` = ?', [1]);
