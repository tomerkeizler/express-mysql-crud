/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import raw from "../../middleware/route.async.wrapper.mjs";
import db from "../../db/mysql.connection.mjs";
import express from "express";
import log from "@ajar/marker";
import { optional_user_schema, required_user_schema } from "./user.schema.mjs";
import validation_handler from "../../middleware/validation.handler.mjs";

const router = express.Router();

// parse json req.body on post routes
router.use(express.json());

// CREATES A NEW USER
router.post(
  "/",
  validation_handler(required_user_schema),
  raw(async (req, res) => {
    log.obj(req.body, "create a user, req.body:");

    const sql = `INSERT INTO users SET ?`;
    const [result] = await db.query(sql, req.body);

    if (result.affectedRows) {
      res.status(200).json(`User added successfully`);
    } else {
      res.status(404).json({ status: `Error while adding user` });
    }
  })
);

// GETS ALL USERS
router.get(
  "/",
  raw(async (req, res) => {
    const sql = `SELECT * FROM users`;
    const [rows] = await db.query(sql);
    res.status(200).json(rows);
  })
);

// GETS ALL USERS - WITH PAGINATION
router.get(
  "/paginate/:page?/:items?",
  raw(async (req, res) => {
    let { page = 0, items = 10 } = req.params;
    const sql = `SELECT * FROM users LIMIT ${page*items}, ${items}`;
    const [rows] = await db.query(sql);
    res.status(200).json(rows);
  })
);


// GETS A SINGLE USER
router.get(
  "/:id",
  raw(async (req, res) => {
    const sql = `SELECT * FROM users WHERE id = ${req.params.id}`;
    const [rows] = await db.query(sql);

    if (rows.length > 0) {
      res.status(200).json(rows[0]);
    } else {
      res.status(404).json({ status: `User ${req.params.id} was not found` });
    }
  })
);

// UPDATES A FULL SINGLE USER
router.put(
  "/:id",
  validation_handler(required_user_schema),
  raw(async (req, res) => {
    const { first_name, last_name, email, phone } = req.body;
    const sql = `UPDATE users SET first_name='${first_name}', last_name='${last_name}', email='${email}', phone='${phone}' WHERE id = ${req.params.id}`;
    const [result] = await db.query(sql);

    if (result.affectedRows) {
      res.status(200).json("User was updated successfully");
    } else {
      res.status(404).json({ status: `User ${req.params.id} was not found` });
    }
  })
);

// DELETES A USER
router.delete(
  "/:id",
  raw(async (req, res) => {
    const sql = `DELETE FROM users WHERE id = ${req.params.id}`;
    const [result] = await db.query(sql);

    if (result.affectedRows) {
      res.status(200).json("User was deleted successfully");
    } else {
      res.status(404).json({ status: `User ${req.params.id} was not found` });
    }
  })
);


export default router;
